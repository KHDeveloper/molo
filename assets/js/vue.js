let app = new Vue({
    el: "#product",
    data: {
        selectedVariant: 0,
        selectedVariantDefault: 0,
        price: 100,
        qty: 200,
        details: ["Good product", "Good shop"],
        brand: "Thailand",
        product: "sock",
        variants: [
            {
                variantId: 1,
                variantColor: "red",
                variantImage: "assets/image/2.jpg",
                variantQty: 1,
            },
            {
                variantId: 2,
                variantColor: "green",
                variantImage: "assets/image/red.png",
                variantQty: 0,
            }
        ],
        card: 0,
    },
    methods: {
        addToCard () {
            this.card +=1
        },
        productUpdate(index) {
            this.selectedVariant = index
        },

    },
    computed: {
        title() {
            return this.brand + ' ' + this.product;
        },
        image() {
            return this.variants[this.selectedVariant].variantImage
        },
        stock() {
            return this.variants[this.selectedVariant].variantQty
        }
    }
});